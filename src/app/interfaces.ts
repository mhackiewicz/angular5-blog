export interface Posts {
    title: string;
    author: string;
}


export interface Comments {
    body: string;
}