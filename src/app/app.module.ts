import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
	
import 'rxjs/Rx';

import { AppComponent } from './app.component';
import { MainService } from './main.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    MainService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
