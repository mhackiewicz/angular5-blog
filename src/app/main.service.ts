import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { map } from 'rxjs/operators';
import { Posts, Comments } from './interfaces';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class MainService {

  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get(`${environment.api}/posts`);
  }

  getComments():Observable<Comments[]> {
    return this.http.get<Comments[]>(`${environment.api}/comments`);
  }
 
}
