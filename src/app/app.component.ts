import { Component, OnInit } from '@angular/core';
import { MainService } from './main.service';
import { Posts, Comments } from './interfaces';
import { Observable } from 'rxjs/Observable';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  posts: Posts[] = [];
  comments$: Observable<Comments[]>;
  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.mainService.getPosts().subscribe(this.onSuccessPost, this.onErrorPost);
    this.comments$ = this.mainService.getComments();
  }

  onSuccessPost = (data) => {
    this.posts = data;
    console.log(this.posts);
  }

  onErrorPost = (error) => {
    console.error(`This is error: ${error}`);
  }

}
